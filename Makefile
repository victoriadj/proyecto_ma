default_target: pdf
.PHONY: default_target

LOOPS = 3

pdf: build/presentation.pdf build/report.pdf
.PHONY: pdf

html: build/presentation.html build/report.html
.PHONY: html

build:
	mkdir build

build/presentation.pdf: | build
	cd src && \
		pdflatex \
			-interaction=nonstopmode \
			-halt-on-error \
			-jobname presentation \
			-output-directory ../build \
			main.tex && \
		pdflatex \
			-interaction=nonstopmode \
			-halt-on-error \
			-jobname presentation \
			-output-directory ../build \
			main.tex && \
		pdflatex \
			-interaction=nonstopmode \
			-halt-on-error \
			-jobname presentation \
			-output-directory ../build \
			main.tex

build/report.pdf: | build
	cd report && \
		pdflatex \
			-interaction=nonstopmode \
			-halt-on-error \
			-jobname report \
			-output-directory ../build \
			main.tex && \
		pdflatex \
			-interaction=nonstopmode \
			-halt-on-error \
			-jobname report \
			-output-directory ../build \
			main.tex && \
		pdflatex \
			-interaction=nonstopmode \
			-halt-on-error \
			-jobname report \
			-output-directory ../build \
			main.tex

build/presentation.html: | build
	cd build && \
	pdf2htmlEX \
		--embed cfijo \
		--printing 0 \
		--optimize-text 1 \
		--fit-width 1200 \
		presentation.pdf

build/report.html: | build
	cd build && \
	pdf2htmlEX \
		--embed cfijo \
		--printing 0 \
		--optimize-text 1 \
		--fit-width 1200 \
		report.pdf

clean:
	rm -r build
.PHONY: clean
